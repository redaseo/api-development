'use strict';

const API = require('./Core/API');
const Errors = require('./Core/errors');
const {webSocketStatus, transactionStatus, orderType, constant} = require('./Core/constants');


/**
 * Exchange API
 * Method descriptions are in parent API class
 * @class
 */
class ExchangeAPI extends API {

    constructor(name, apiKey, apiSecret, apiPassphrase) {
		super(name, apiKey, apiSecret, apiPassphrase);
		// constructor implementation
    }

	onOpen(wsData) {
		// method implementation, can be empty
	}

	onMessage(wsData, message) {
		// method implementation
    }

	onClose(wsData) {
		// method implementation, can be empty
	}

    async ping(wsData) {
        // ...implementation
    }

    async getExchangeStatus(wsData) {
		// ...implementation
	}

    async authorize(wsData) {
		// ...implementation
	}

    async subscribeOrderbooks(wsData, symbols) {
        // ...implementation
	}

    async makeOrder(wsData, symbol, amount, bid, ask) {
		// ...implementation
	}

    async getDepositInfo(wsData, currency) {
        // ...implementation
    }

    async makeWithdraw(wsData, depositInfo, amount) {
		// ...implementation
   }

    async getWithdrawHistory(wsData, currency = null, id = null, limit = constant.HISTORY_LIMIT) {
		// ...implementation
	}

    async getDepositHistory(wsData, currency = null, id = null, limit = constant.HISTORY_LIMIT) {
		// ...implementation
    }

    async getBalance(wsData) {
		// ...implementation
	}

    async makeInternalTransfer(wsData, currency, amount, type) {
		// ...implementation
	}

    async getTradeHistory(wsData, symbol, orderId = null, limit = constant.HISTORY_LIMIT) {
		// ...implementation
	}

}


module.exports = ExchangeAPI;
