'use strict';

const Request = require('request');
const {terminationInit, createAPI, createWebSocket, stringify} = require('./Core/core');
const ExchangeAPI = require('./ExchangeAPI');
const {transferType} = require('./Core/constants');
const config = require('./config');


(async() => {

    // Create exchange API
    let exchangeAPI = createAPI('Exchange', ExchangeAPI, config.credentials);


    // WebSocket and REST connections
    let wsData = {
        ws: null,
        wsUrl: 'wss://ws.gate.io/v3/',
        rest: Request,
        restUrl: 'https://api.gateio.co/',
        id: 1, // not used here
        reconnectionTimer: null, // not used here
        error: false, // not used here
        isApiExplicitlyClosed: false // not used here
    };

    try {

        // Create WebSocket connection and execute code on WebSocket open event
        createWebSocket(wsData, exchangeAPI, () => {

            /********** Exchange API WebSocket methods **********/

            // // Ping/pong
            // let pong = await exchangeAPI.ping(wsData);
            // console.log('Pong:', stringify(pong));

            // // Subscribe to symbol
            // let subscribedSymbols = exchangeAPI.subscribeOrderbooks(wsData, ['XRPBTC']);
            // console.log('Subscribed symbols:', stringify(subscribedSymbols));

            /***************************************************/

        });

        /********** Exchange API methods (call examples) **********/

        // // Get deposit information
        // const depositInfo = await exchangeAPI.getDepositInfo(wsData, 'XRP');
        // console.log('Deposit address:', stringify(depositInfo));

        // // Get exchange status
        // const exchangeStatus = await exchangeAPI.getExchangeStatus(wsData);
        // console.log('Exchange status:', stringify(exchangeStatus));

        // // Get balance
        // const balance = await exchangeAPI.getBalance(wsData);
        // console.log('Balance:', stringify(balance))

        // // Get trade history
        // const tradeHistory = await exchangeAPI.getTradeHistory(wsData, 'xrp_btc', null, 5);
        // console.log('Trade history:', stringify(tradeHistory));
        // console.log('Trade history size:', tradeHistory.length);

        // // Make order
        // let order = await exchangeAPI.makeOrder(wsData, 'XRP_BTC', -20, 'number', 'number');
        // console.log('Order:', stringify(order));

        // // Withdraw
        // let transaction = await exchangeAPI.makeWithdraw(wsData, {currency: 'XRP', address: 'address', payment_id: 'additional address'}, 50);
        // console.log('Withdraw transaction:', stringify(transaction));

        // // Make internal transfer
        // let transfer = await makeInternalTransfer(wsData, 'XRP', 20, transferType.MAIN_TO_TRADING);
        // console.log('Transfer:', stringify(transfer));

        // // Get deposit history
        // const depositHistory = await exchangeAPI.getDepositHistory(wsData, 'XRP', null, 1);
        // console.log('Deposit history:', stringify(depositHistory));
        // console.log('Deposit history size:', depositHistory.length);

        // // Get withdraw history
        // const withdrawHistory = await exchangeAPI.getWithdrawHistory(wsData, 'XRP', null, 1);
        // console.log('Withdraw history:', stringify(withdrawHistory));
        // console.log('Withdraw history size:', withdrawHistory.length);

        /********************************************************/

    } catch (error) {
        console.log(error);
    }

    terminationInit(wsData);

})();
