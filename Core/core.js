'use strict';

const WebSocket = require('ws');


/**
 * Short version of JSON.stringify
 * @param {object} str - string to stringify
 * @return {string}
 */
let stringify = (str) => {
    return JSON.stringify(str, null, 4);
};
exports.stringify = stringify;


/**
 * SIGINT initialization
 * @param {object} wsData
 */
exports.terminationInit = (wsData) => {
    process.on('SIGINT', () => {
        wsData.ws.close();
        setTimeout(() => {
            console.log('SIGINT: forcing shut down');
            process.exit();
        }, 1000)
    });
};


/**
 * @typedef {Object} CredentialObject
 * @property {string} apiKey - API key
 * @property {string} apiSecret - API secret
 * @property {string} apiPassphrase - API passphrase
 */
/**
 * Create API class instance
 * @param {string} name - exchange name
 * @param {object} Api - API class
 * @param {CredentialObject} credentials - exchange credential
 * @return {object}
 */
exports.createAPI = (name, Api, credentials) => {
    let exchangeAPI = new Api(name, credentials.apiKey, credentials.apiSecret, credentials.apiPassphrase);

    exchangeAPI.on('error', (name, error) => console.log('Error', stringify(error)));
    exchangeAPI.on('updateOrderBook', (name, symbol, type, price, amount) => console.log('Update', stringify({name, symbol, type, price, amount})));
    exchangeAPI.on('deleteOrderBook', (name, symbol, type) => console.log('Delete', stringify({name, symbol, type})));

    return exchangeAPI;
};


/**
 * @callback Callback
 */
/**
 * Create WebSocket connection and attach WebSocket events
 * @param {object} wsData
 * @param {object} exchangeAPI - API class instance
 * @param {Callback|null} onOpenCb - callback function
 * @param {Callback|null} onMessageCb - callback function
 * @param {Callback|null} onErrorCb - callback function
 * @param {Callback|null} onCloseCb - callback function
 * @return {object}
 */
exports.createWebSocket = (wsData, exchangeAPI, onOpenCb = null, onMessageCb = null, onErrorCb = null, onCloseCb = null) => {
    wsData.ws = new WebSocket(wsData.wsUrl);

    wsData.ws.on('message', message => {
        exchangeAPI.onMessage(wsData, message);
        if (onMessageCb) onMessageCb();
    });

    wsData.ws.on('error', error => {
        console.log('WebSocket error:', stringify(error));
        if (onErrorCb) onErrorCb();
    });

    wsData.ws.on('close', () => {
        console.log('Connection to exchange is closed');
        exchangeAPI.onClose(wsData);
        if (onCloseCb) onCloseCb();
    });

    wsData.ws.on('open', () => {
        console.log('Connection to exchange is opened');
        exchangeAPI.onOpen(wsData);
        if (onOpenCb) onOpenCb();
    });

    return wsData;
};

