'use strict';


/**
 * Scheme
 * @class
 */
class Scheme {

    /**
     * Constructor
     * @constructs Scheme
     */
    constructor() {

        // Constants
        Scheme.DEPOSIT_INFO = 1;

    }

    /**
     * Validate object
     * @param {number} type - validation scheme
     * @param {Array<Object|string>} object - object to validate
     * @return {boolean}
     */
    validate(type, object) {

        switch (type) {
            case Scheme.DEPOSIT_INFO:
                return this.depositInfo(object);

        }

    }


    /**
     * Validate depositInfo method arguments
     * @param {Array<Object|string>} args - method arguments
     * @return {boolean}
     */
    depositInfo(args) {
        return true;
    }

}


module.exports = Scheme;
