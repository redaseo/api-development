'use strict';

const EventEmitter = require('events');
const {webSocketStatus, transactionStatus, orderType, constant} = require('./constants');
// const Scheme = require('./Scheme');


/**
 * Base API class
 * @class
 */
class API extends EventEmitter {

    /**
     * Parent constructor
     * @constructs API
     * @param {string} name - exchange name
     * @param {string} apiKey - API key
     * @param {string} apiSecret - secret key
     * @param {string} apiPassphrase - password phrase
     */
    constructor(name, apiKey, apiSecret, apiPassphrase) {
        super();

        this.name = name;
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
        this.apiPassphrase = apiPassphrase;
    }


    /**
     * Get exchange name
     * @return {string}
     */
    getName() {
        return this.name;
    }


    /**
     * Reject
     */
    reject(error) {
        return Promise.reject(new Error(error));
    }


    /**
     * @typedef {Object} WSObject
     * @property {number} id - unique number of WebSocket connection
     * @property {WebSocket} ws - WebSocket object (WebSocket protocol has 4 states: connecting (readyState = 0), open (readyState = 1), closing (readyState = 2), closed (readyState = 3))
     * @property {string} wsUrl - WebSocket url
     * @property {Request} rest - Request object
     * @property {string} restUrl - REST API url
     * @property {Object} reconnectionTimer - object returned by method setTimeout
     * @property {boolean} error - true if error occurs in WebSocket connection
     * @property {boolean} isApiExplicitlyClosed - true if WebSocket connection was closed explicitly
     */
    /**
     * Called on WebSocket open event
     * @param {WSObject} wsData - WebSocket connection data
     */
    onOpen(wsData) {}


    /**
     * Called when WebSocket message event occurred
     * @param {WSObject} wsData - WebSocket connection data
     * @param message - message received from exchange
     */
    onMessage(wsData, message) {}


    /**
     * Called on WebSocket close event
     * @param {WSObject} wsData - WebSocket connection data
     */
    onClose(wsData) {}


    /**
     * Add order or modify existing order
     * @param {string} symbol - exchange symbol
     * @param {string} type - "bid" or "ask"
     * @param {number} price - order price
     * @param {amount} amount - amount
     */
    updateOrderBook(symbol, type, price, amount) {
        this.emit('updateOrderBook', this.getName(), symbol, type, price, amount);
    }


    /**
     * Delete order
     * @param {string} symbol - exchange symbol
     * @param {string} type - "bid" or "ask"
     * @param {number} price - order price
     */
    deleteOrderBook(symbol, type, price) {
        this.emit('deleteOrderBook', this.getName(), symbol, type, price);
    }


    /**
     * Clear orderbook by symbol
     * @param {string} symbol - exchange symbol
     */
    clearOrderBook(symbol) {
        this.emit('clearOrderBook', this.getName(), symbol);
    }


    /**
     * Must be called when API gets an error
     * @param {string} message - (optional) message
     * @param {object} error - error object
     */
    error(error, message = '') {
        this.emit('error', this.getName(), error, message);
    }


    /**
     * @typedef {Object} ErrorObject
     * @property {object} error - error
     * @property {string} message - error message
     */

    /**
     * @typedef {Object} PongObject
     * @property {string} result - 'pong' message
     */
    /**
     * Make ping request to WebSocket connection
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @return {Promise.<PongObject | ErrorObject>}
     */
    async ping(wsData) {}


    /**
     * @typedef {Object} StatusObject
     * @property {boolean} status - exchange status
     */
    /**
     * Get exchange status
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @return {Promise.<StatusObject | ErrorObject>}
     */
    async getExchangeStatus(wsData) {}


    /**
     * Authorization
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @return {Promise.<StatusObject | ErrorObject>}
     */
    async authorize(wsData) {}


    /**
     * @typedef {Object} SubscriptionObject
     * @property {Array<string>} symbols - array of symbols
     */
    /**
     * Subscribe to order books
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @param {Array<string>} symbols - array of symbols
     * @return {Promise.<SubscriptionObject | ErrorObject>}
     */
    async subscribeOrderbooks(wsData, symbols) {}


    /**
     * @typedef {Object} OrderObject
     * @property {string} orderId - exchange orderId
     */
    /**
     * Make order
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @param {string} symbol - cryptocurrency pair, example: "ETH/BTC"
     * @param {number} amount - cryptocurrency amount
     * @param {number} bid - bid price
     * @param {number} ask - ask price
     * @return {Promise.<OrderObject | ErrorObject>}
     */
    async makeOrder(wsData, symbol, amount, bid, ask) {}


    /**
     * @typedef {Object} DepositObject
     * @property {string} currency - currency, example: "BTC", "ETH"
     * @property {string} address - wallet address
     * @property {string | null} payment_id - If set, it is required for deposit
     */
    /**
     * Get information about account
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @param {string} currency - currency, example: "BTC", "ETH"
     * @return {Promise.<DepositObject | ErrorObject>}
     */
    async getDepositInfo(wsData, currency) {}


    /**
     * @typedef {Object} TransactionObject
     * @property {string | null} id - transaction id (null if exchange does not provide any transaction id)
     */
    /**
     * Withdraw
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @param {DepositObject} depositInfo - information about account where currency is sending
     * @param {number} amount - cryptocurrency volume
     * @return {Promise.<TransactionObject | ErrorObject>}
     */
    async makeWithdraw(wsData, depositInfo, amount) {}


    /**
     * @typedef {Object} HistoryObject
     * @property {string} id - transaction unique identifier as assigned by exchange
     * @property {string} currency - currency, example: "BTC", "ETH"
     * @property {number} amount - amount
     * @property {number} status - transaction status (1 is "pending", 2 is "failed", 3 is "completed")
     * @property {string | null} address - destination address (null if no address provided - can be on deposit history)
     * @property {string | null} payment_id - additional address (null if no additional address provided)
     * @property {string} hash - blockchain transaction hash (same as TXID or transaction id)
     */
    /**
     * Get withdraw history
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @param {string | null} currency - currency, example: "BTC", "ETH"
     * @param {string | null} id - transaction id assigned by exchange
     * @param {number} limit - number of deposit records
     * @return {Promise.<Array<HistoryObject>>}
     */
    async getWithdrawHistory(wsData, currency = null, id = null, limit = constant.HISTORY_LIMIT) {}


    /**
     * @typedef {Object} HistoryObject
     * @property {string} id - transaction unique identifier as assigned by exchange
     * @property {string} currency - currency, example: "BTC", "ETH"
     * @property {number} amount - amount
     * @property {number} status - transaction status (1 is "pending", 2 is "failed", 3 is "done", 4 is "unknown")
     * @property {string} address - destination address
     * @property {string} hash - blockchain transaction hash (same as TXID or transaction id)
     */
    /**
     * Get deposit history
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @param {string | null} currency - currency, example: "BTC", "ETH"
     * @param {string | null} id - transaction id assigned by exchange
     * @param {number} limit - number of deposit records
     * @return {Promise.<Array<HistoryObject>>}
     */
    async getDepositHistory(wsData, currency = null, id = null, limit = constant.HISTORY_LIMIT) {}


    /**
     * Get exchange balance (indexes are currencies, values are amounts)
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @return {Promise.<Array<string, Number>>}
     */
    async getBalance(wsData) {}


    /**
     * Make internal transfer between internal accounts
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @param {string} currency - cryptocurrency
     * @param {number} amount - amount of transfer
     * @param {number} type - type of transfer (see transferType in constants)
     * @return {Promise.<TransactionObject>}
     */
    async makeInternalTransfer(wsData, currency, amount, type) {}


    /**
     * @typedef {Object} TradeObject
     * @property {string} id - trade id
     * @property {string} orderId - order id
     * @property {string} symbol - exchange symbol
     * @property {number} price - price
     * @property {number} amount - amount
     * @property {number} timestamp - trade timestamp
     * @property {string} type - 'buy' or 'sell'
     * @property {number | null} fee - trade fee
     */
    /**
     * Get all trades of specific order ID and symbol
     * @async
     * @param {WSObject} wsData - WebSocket connection data
     * @param {string} symbol - exchange symbol (if not null, filter by symbol)
     * @param {string | null} orderId - order ID (if not null, filter by order ID)
     * @param {number} limit - number of trade records
     * @return {Promise.<Array<TradeObject>>}
     */
    async getTradeHistory(wsData, symbol, orderId = null, limit = constant.HISTORY_LIMIT) {}

}


module.exports = API;