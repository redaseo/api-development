'use strict';

module.exports = {

    syntax: {
        BAD_PARAMS: 'Bad method params'
    },

    http: {
        RESPONSE_ERROR: 'Response error', //called when Request library returns error object
        UNEXPECTED_RESPONSE: 'Unexpected response', //called when unexpected data is returned
        WRONG_JSON: 'Wrong json', //called when JSON parsing with errors
        API_ERROR: 'Exchange API error', //called when exchange API sends error
        UNKNOWN_ERROR: 'Unknown error' //other errors
    },

    depositInfo: {
        BAD_CURRENCY: 'Bad currency'
    },

    ping: {
        ALREADY_EXECUTED: 'Ping/pong already executed',
        TIMEOUT: 'Ping timeout',
        API_ERROR: 'Error in response message'
    },

    tradeHistory: {
        BAD_CURRENCY: 'Bad currency'
    },

    subscribeOrderbooks: {
        FAILED: 'Subscription is failed'
    }

};