'use strict';

exports.webSocketStatus = {
    CONNECTING: 0,
    OPEN: 1,
    CLOSING: 2,
    CLOSED: 3
};

exports.transactionStatus = {
    PENDING: 1,
    FAILED: 2,
    DONE: 3,
    UNKNOWN: 4
};

exports.orderType = {
    BID: 'bid',
    ASK: 'ask'
};

exports.constant = {
    PONG: 'pong',
    HISTORY_LIMIT: 50
};

exports.transferType = {
    MAIN_TO_TRADING: 1,
    TRADING_TO_MAIN: 2
};